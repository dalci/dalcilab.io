---
title: pacman repository
---

### How to use it?

- First, install the primary key - it can then be used to install the keyring:

  ~~~{ .sh}
  pacman-key --recv-key 5874D2437CD5BBB3 --keyserver keyserver.ubuntu.com
  pacman-key --lsign-key 5874D2437CD5BBB3
  ~~~

- Append (adding to the **end** of the file) to `/etc/pacman.conf`:
 
  ~~~{ .ini title="/etc/pacman.conf"}
  [doppelhelix-arch-repo]
  SigLevel = Required DatabaseOptional
  Server = https://gitlab.com/doppelhelix-arch/$repo/-/raw/main/$arch
  ~~~

- Sync repositories and update your system:

  ~~~{ .sh}
  sudo pacman -Syyu
  ~~~

- Install the Keyring

  ~~~{ .sh}
  sudo pacman -S dalci-keyring
  ~~~
