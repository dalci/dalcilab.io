---
title: archinstall

---
# Archlinux with btrfs and snapper on GNOME
???+ Info "Pre-installation" 
    This guide assumes that you already booted into your live environment as described in the [Archlinux Installation guide](https://wiki.archlinux.org/title/Installation_guide){target="_blank"}.

To keep things simple, [`archinstall`](https://wiki.archlinux.org/title/archinstall){target="_blank"} is used.

---

## ***Part 1:*** Installation
### Keyboard layout
- This guide is centered around the german language. If you need another keyboard layout, change the commands accordingly.
~~~sh
loadkeys de-latin1
~~~
  
### Wifi
~~~sh
iwctl device list
iwctl station <DEVICE> scan
iwctl station <DEVICE> get-networks
iwctl --passphrase=<PASSPHRASE> station <DEVICE> connect <SSID>
~~~

### Start the installer
~~~sh
archinstall
~~~

### Language of the installer
~~~ { .txt .no-copy }
Archinstall language = English
~~~

### German language pack
~~~ { .txt .no-copy }
Keyboard layout = de
Mirror region = Germany
Locale Language = de_DE.UTF-8
Locale encoding = UTF-8
~~~

### Drive to install the system to (I assume you know your drives?!)
~~~ { .txt .no-copy }
Drives = <CHOOSE_YOUR_INSTALL_DRIVE>
~~~

### Disk layout
~~~ { .txt .no-copy }
Disk layout = <HERE BE DRAGONS>
~~~
#### First partition
~~~ { .txt .no-copy }
fat32
  startlocation = 1MiB
  endlocation = 4098MiB
  mountpoint = /boot
~~~
#### Second partition
~~~ { .txt .no-copy }
btrfs
  startlocation = 4099MiB
  endlocation = 100%
~~~
#### btrfs subvolumes
~~~ { .txt .no-copy }
  Subvolumes 
  #escape out of compress, archinstall doesn't enable it anyway (2023-06-2023)

    @ = /
    @home = /home
    @swap = /swap
    @snapshots = /.snapshots
    @var_log = /var/log
    @pkg = /var/cache/pacman/pkg
    @images = /var/lib/libvirt/images
# IMPORTANT
      nodatacow = yes

# gdm needs to be rw if we boot from snapshots
# This is why we make the following subvolumes
    @AccountsService = /var/lib/AccountsService
    @gdm = /var/lib/gdm
~~~

!!! Note ""
    The subvolumes `@AccontService` and `@gdm` will ensure that [`gdm`](https://wiki.archlinux.org/title/GDM){target="_blank"} works if booted into a [snapshot](https://wiki.archlinux.org/title/Btrfs#Snapshots){target="_blank"} and no [overlayfs](https://wiki.archlinux.org/title/Overlay_filesystem){target="_blank"} is present.

### Bootloader
- [GRUB](https://wiki.archlinux.org/title/GRUB){target="_blank"} since we will use [`grub-btrfs`](https://archlinux.org/packages/?name=grub-btrfs){target="_blank"} to create bootmenus for snapshots

~~~ { .txt .no-copy }
Bootloader
  GRUB = yes
~~~

### Hostname
~~~ { .txt .no-copy }
Hostname = <HOSTNAME>
~~~

### Root account
~~~ { .txt .no-copy }
Root passwort = none
~~~

### User account 
- Create with [`sudo`](https://wiki.archlinux.org/title/Sudo){target="_blank"} priviliges

~~~ { .txt .no-copy }
User account
  Add a user
    Enter username = <USERNAME>
    Password = <PASSWORT>

    Superuser = yes
~~~

### GNOME and open-source graphic driver
~~~ { .txt .no-copy }
Profile 
  desktop
    gnome
      Graphics driver = All open-source
~~~

### Audio-backend
~~~ { .txt .no-copy }
Audio = pipewire
~~~

### Kernel
~~~ { .txt .no-copy }
Kernels = linux-zen
~~~

### Additional packages
~~~
[amd|intel]-ucode cups firefox firefox-i18n-de flatpak git inotify-tools nano noto-fonts reflector ttf-dejavu ttf-jetbrains-mono ttf-liberation dconf-editor file-roller p7zip unrar gedit gedit-plugins seahorse gnome-terminal gnome-tweaks libreoffice-fresh libreoffice-fresh-de meld gimp inkscape gparted dosfstools btrfs-progs ntfs-3g polkit gpart mtools xorg-xhost vlc lib32-gnutls wine
~~~

### Networking
~~~ { .txt .no-copy }
Network configuration = NetworkManager
~~~

### Timezone
~~~ { .txt .no-copy }
Timezone = Europe/Berlin
~~~

### Optional repositories
~~~ { .txt .no-copy }
Optional repositories = multilib
~~~
### Start the installation
!!! Warning ""
    **No way back after this point.**

~~~ { .txt .no-copy }
Install
~~~
- Wait for the installer to finish without error

## ***Part 2:*** chroot-environment

### chroot
- [`chroot`](https://wiki.archlinux.org/title/chroot){target="_blank"} into the new system

  ~~~ { .txt .no-copy }
  chroot = yes
  ~~~


#### Fix `fstab`
- **remove** from all btrfs-partitions 

  ~~~ { .no-copy title="/etc/fstab"}
  space_cache=v2,subvolid=<ID>
  ~~~

- **add** to all btrfs-partitions

  ~~~ { .no-copy title="/etc/fstab"}
  compress=zstd
  ~~~


#### Compress btrfs
- The filesystem is still [uncompessed](https://wiki.archlinux.org/title/Btrfs#Compression){target="_blank"}

  ~~~sh
  btrfs filesystem defragment -r -v -czstd /
  ~~~

#### Enable systemd services
- Archlinux doesn't enable these [systemd](https://wiki.archlinux.org/title/Systemd){target="_blank"} services by default

  ~~~sh
  systemctl enable avahi-daemon
  systemctl enable bluetooth
  systemctl enable cups
  systemctl enable upower
  systemctl enable sshd
  ~~~

#### Configure and enable Reflector

- Uncomment and change

  ~~~{ .no-copy title="/etc/xdg/reflector/reflector.conf"}
  --Country <YOUR_COUNTRY>
  --sort rate
  ~~~

- enable [systemd](https://wiki.archlinux.org/title/Systemd){target="_blank"} timer

  ~~~sh
  systemctl enable reflector.timer
  ~~~

### Exit chroot and reboot
~~~sh
exit
reboot
~~~

## ***Part 3:*** Inside the new ArchLinux system

### Check /etc/fstab

- `compress=zstd:3` and `space_cache=v2` should be added automatically.

  ~~~sh
  mount | grep home
  ~~~


### Update the system
- Check for Updates

  ~~~sh
  sudo pacman -Syy
  sudo pacman -Syu
  ~~~

### KVM/QEMU
- If the system resides within a virtual machine, enable [dynamic resolution and clipboard-sharing](https://wiki.archlinux.org/title/QEMU#Enabling_SPICE_support_on_the_guest){target="_blank"}

  ~~~sh
  sudo pacman -S spice-vdagent
  ~~~

- Reboot the system

  ~~~sh
  reboot
  ~~~

### Enable Arch User Repository (AUR)
- Take a look at the [yay Installation guide](https://github.com/Jguer/yay#installation){target="_blank"}

  ~~~sh
  git clone https://aur.archlinux.org/yay
  cd yay
  makepkg -si
  cd ..
  rm -rf yay
  
  yay -Y --gendb #generate a development package database for *-git packages
  yay -Syu --devel
  yay -Y --devel --combinedupgrade --batchinstall --editmenu --nodiffmenu --save
  ~~~


### Create and manage snapshots

- Install the [`snapper-support`](https://aur.archlinux.org/packages/snapper-support){target="_blank"} metapackage.</br>
This will pull and configure [`snapper`](https://wiki.archlinux.org/title/snapper){target="_blank"}, [`grub-btrfs`](https://archlinux.org/packages/?name=grub-btrfs){target="_blank"} and [`snap-pac`](https://archlinux.org/packages/?name=snap-pac){target="_blank"}.

  ~~~sh
  yay -S snapper-support
  ~~~
!!! Warning "IMPORTANT"
    
    **[snapper-support](https://aur.archlinux.org/packages/snapper-support){target="_blank"} fails to create a btrfs subvolume.**</br>**This is expected.**</br>**Go to [Fix snapshots](#fix-snapshots)**
    
    **[grub-btrfs](https://archlinux.org/packages/?name=grub-btrfs){target="_blank"} doesn't work with systemd ([issue #199](https://github.com/Antynea/grub-btrfs/issues/199){target="_blank"}).**</br>**This is expected.**</br>**Go to [Fix initial ramdisk](#fix-initial-ramdisk)**


### Fix initial ramdisk
- [`grub-btrfs`](https://archlinux.org/packages/?name=grub-btrfs){target="_blank"} doesn't work with `systemd` hook ([issue #199](https://github.com/Antynea/grub-btrfs/issues/199){target="_blank"})
</br></br>
- Edit `/etc/mkinitcpio.conf` to change from systemd to udev:
</br></br>
Change the following in `HOOKS`:</br>
**replace** ***`systemd`*** with ***`udev`***</br>
**replace** ***`sd-vconsole`*** with ***`keymap consolefont`***
</br></br>
Change the following in `BINARIES`:</br>
**add** ***`setfont`***
</br></br>
Edit `/etc/vconsole.conf`</br>
FONT is missing and `eurlatgr` is a better default console font for European based languages.)
</br>
`FONT=eurlatgr`

- Regenerate initial ramdisk

  ~~~sh
  sudo mkinitcpio -P
  ~~~


### Fix snapshots
- Umount and delete ./snapshots

  ~~~sh
  sudo umount /.snapshots
  sudo rm -r /.snapshots
  ~~~
- Create a new configuration

  ~~~sh
  sudo snapper -c root create-config /
  ~~~
- delete /.snapshots subvolume

  ~~~sh
  sudo btrfs subvolume delete /.snapshots
  ~~~

- check if /.snapshots is gone and @snapshots is present

  ~~~sh
  sudo btrfs subvolume list /  
  ~~~

- create and mount /.snapshots

  ~~~sh
  sudo mkdir /.snapshots
  sudo mount -av
  ~~~

- A good practice is to reboot now

  ~~~sh
  reboot
  ~~~

### Change btrfs default subvolume to @
- The default subvolume should not point to '/'

  ~~~sh
  sudo btrfs subvol get-default /
  ~~~
- this should return somthing like

  ~~~
  ID 5 (FS_TREE)
  ~~~
- we need to change that

  ~~~sh
  sudo btrfs subvol list /
    # take a note of the ID of subvolume @ (possibly 256)
    # change the command accordingly
  sudo btrfs subvolume set-default 256 /
    # check again with
  sudo btrfs subvol get-default /
  ~~~

### Enable swap
@swap is already mounted at /swap. See [Chapter 8. Disk layout](#disk-layout)

- create a swapfile

  ~~~sh
  btrfs filesystem mkswapfile --size 4g --uuid clear /swap/swapfile
  swapon /swap/swapfile
  ~~~

- edit the [fstab](https://wiki.archlinux.org/title/Fstab){target="_blank"} configuration to add an entry for the swap file:

  ~~~ { .no-copy title="/etc/fstab"}
  /swap/swapfile none swap defaults 0 0
  ~~~

### Change permissions for snapper and /.snapshots

- Allow members of [`wheel`](https://wiki.archlinux.org/title/Users_and_groups#Group_list){target="_blank"} access to snapper

  ~~~ { .no-copy title="/etc/snapper/configs/root"}
  ALLOW_GROUPS="wheel"
  ~~~

- Allow members of [`wheel`](https://wiki.archlinux.org/title/Users_and_groups#Group_list){target="_blank"} access to /.snapshots

  ~~~
  sudo chown -R :wheel /.snapshots
  ~~~
### Set snapshot limits
- Set limits to the Arch-wiki [recommendation](https://wiki.archlinux.org/title/snapper#Set_snapshot_limits){target="_blank"}

  ~~~
  TIMELINE_MIN_AGE="1800"
  TIMELINE_LIMIT_HOURLY="5"
  TIMELINE_LIMIT_DAILY="7"
  TIMELINE_LIMIT_WEEKLY="0"
  TIMELINE_LIMIT_MONTHLY="0"
  TIMELINE_LIMIT_YEARLY="0"
  ~~~

### Preventing slowdowns
- See [preventing slowdowns](https://wiki.archlinux.org/title/snapper#Preventing_slowdowns){target="_blank"} in the Arch-wiki
